package com.zlp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author ：ZouLiPing
 * @description：启动类
 * @date ： 2019/8/27 17:42
 */
@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients
public class MemberApp {

    public static void main(String[] args) {
        SpringApplication.run(MemberApp.class,args);
    }
}
