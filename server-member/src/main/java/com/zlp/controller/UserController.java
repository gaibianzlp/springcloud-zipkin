package com.zlp.controller;

import brave.Span;
import brave.Tracer;
import com.zlp.call.IUserServiceFegin;
import com.zlp.entity.UserDo;
import com.zlp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author ：ZouLiPing
 * @description：User
 * @date ： 2019/8/28 10:44
 */
@RestController
public class UserController {

    @Autowired
    private Tracer tracer;
    
    @Resource
    private IUserServiceFegin userServiceFegin;

    @GetMapping("getUserList")
    private List<UserDo> getUserList(){
        Span span = tracer.currentSpan();
        String traceId = span.context().traceIdString();
        System.out.println(traceId);
        List<UserDo> userList = userServiceFegin.getUserList();
        return userList;
    }


}
