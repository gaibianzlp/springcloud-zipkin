package com.zlp.call;

import com.zlp.entity.UserDo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

/**
 * @author ：ZouLiPing
 * @description：user远程调用
 * @date ： 2019/8/28 10:37
 */
@FeignClient(name = "app-user")
public interface IUserServiceFegin {

    /**
     * 分页查询平台会员列表
     *
     * @return
     */
    @GetMapping(value = "/user/getUserList")
    List<UserDo> getUserList();

}
