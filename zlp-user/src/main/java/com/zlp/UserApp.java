package com.zlp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author ：ZouLiPing
 * @description：启动类
 * @date ： 2019/8/27 17:45
 */
@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients
        //(basePackages = {"com.zlp.**"})
public class UserApp {

    public static void main(String[] args) {
        SpringApplication.run(UserApp.class,args);
    }
}
