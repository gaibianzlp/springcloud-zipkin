package com.zlp.service;

import com.zlp.entity.UserDo;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface UserService {

    List<UserDo> getUserList();
}
