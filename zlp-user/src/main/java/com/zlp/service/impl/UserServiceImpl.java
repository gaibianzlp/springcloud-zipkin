package com.zlp.service.impl;

import com.zlp.entity.UserDo;
import com.zlp.service.UserService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ：ZouLiPing
 * @description：用户接口实现类
 * @date ： 2019/8/27 17:49
 */
@RestController
@RequestMapping("user")
public class UserServiceImpl implements UserService {


    @GetMapping("getUserList")
    @Override
    public List<UserDo> getUserList() {

        List<UserDo> userDoList = new ArrayList<>();
        UserDo user = new UserDo();
        user.setAccountType("管理员");
        user.setEmail("865391093@qq.com");
        user.setSex(1);
        user.setUserName("zlp");
        userDoList.add(user);
        return userDoList;
    }
}
