package com.zlp.entity;

import lombok.Data;

import java.util.Date;

/**
* Created by Mybatis Generator on 2019/04/01
*/
@Data
public class UserDo {
    private Integer id;

    private String userId;

    private String userName;

    private String userPwd;

    private String accountType;

    private String mobile;

    private String email;

    private Integer sex;

    private Integer isAvailability;

    private Integer isAdmin;

    private String wxOpenId;

    private String qqOpenId;

    private Date createTime;

    private String createBy;

    private Date updateTime;

    private String updateBy;
}